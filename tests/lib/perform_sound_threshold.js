import { getByText, queryByText } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

function press_key(key) {
    document.querySelector('.jspsych-display-element').dispatchEvent(new KeyboardEvent('keydown', { keyCode: key }));
    document.querySelector('.jspsych-display-element').dispatchEvent(new KeyboardEvent('keyup', { keyCode: key }));
}

function perform_sound_threshold(audio_ctx, probe_sound, responses = [], strings = null) {
    let current_response = false;
    let shall_stop = false;

    while (!shall_stop) {
        expect(window.document.querySelector('#jspsych-content'))
            .toBeInTheDocument();
        expect(getByText(window.document, '+'))
            .toBeInTheDocument();
        audio_ctx.processTo(audio_ctx.currentTime + 0.5 + probe_sound.duration);
        expect(() => {
            getByText(window.document, '+');
        })
            .toThrowError();
        expect(getByText(window.document, strings.question))
            .toBeInTheDocument();
        expect(getByText(window.document, strings.yes_response))
            .toBeInTheDocument();
        expect(getByText(window.document, strings.no_response))
            .toBeInTheDocument();

        if (current_response) {
            userEvent.click(getByText(window.document, strings.yes_response));
        } else {
            userEvent.click(getByText(window.document, strings.no_response));
        }

        if (responses.length > 0) {
            current_response = responses.shift();
        } else {
            current_response = !current_response;
        }

        if (queryByText(window.document, 'Done') !== null) {
            expect(getByText(window.document, 'Done'))
                .toBeInTheDocument();
            press_key(32);
            shall_stop = true;
        }
    }
}

export { perform_sound_threshold };
