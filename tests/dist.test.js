import { Sine, Base } from '@thht/sounds';
import '@testing-library/jest-dom/extend-expect';
import { sleepMilliseconds } from 'sleepjs';
import randomstring from 'randomstring';
import { initJsPsych } from 'jspsych';
import HtmlKeyboardResponsePlugin from '@jspsych/plugin-html-keyboard-response';
import { perform_sound_threshold } from './lib/perform_sound_threshold';
import SoundThresholdPlugin from '../dist/sound_threshold';

const AudioContext = require('web-audio-engine').RenderingAudioContext;

const jsPsych = initJsPsych();

describe('test if the plugin runs correctly from dist', () => {
    beforeEach(() => {
        /* eslint-disable global-require, no-undef */
        jsPsych.webaudio_context = new AudioContext();
        Base._global_audio_context = jsPsych.webaudio_context;
        /* eslint-enable */
    });

    test('do two runs and check data run', (done) => {
        // eslint-disable-next-line no-undef
        const audio_ctx = jsPsych.webaudio_context;
        const default_strings = {
            yes_response: 'Yes',
            no_response: 'No',
            question: 'Did you hear the sound?',
        };
        const random_strings = {
            yes_response: randomstring.generate(),
            no_response: randomstring.generate(),
            question: randomstring.generate(),
        };
        const probe_sound = new Sine(440, 1);
        const second_probe_sound = new Sine(330, 1);
        const my_timeline = [
            {
                type: SoundThresholdPlugin,
                sound: () => probe_sound,
                on_start: () => {
                    sleepMilliseconds(100).then(() => {
                        perform_sound_threshold(audio_ctx, probe_sound, [], default_strings);
                    })
                        .catch((e) => { throw e; });
                },
            },
            {
                type: HtmlKeyboardResponsePlugin,
                stimulus: '<p>Done</p>',
            },
            {
                type: SoundThresholdPlugin,
                sound: () => second_probe_sound,
                on_start: () => {
                    sleepMilliseconds(100).then(() => {
                        perform_sound_threshold(audio_ctx,
                            second_probe_sound, [false, false, false, false], random_strings);
                    })
                        .catch((e) => { throw e; });
                },
                yes_response: random_strings.yes_response,
                no_response: random_strings.no_response,
                question: random_strings.question,
            },
            {
                type: HtmlKeyboardResponsePlugin,
                stimulus: '<p>Done</p>',
            },
        ];

        const start_jspsych = function start_jspsych() {
            return new Promise((resolve) => {
                jsPsych.getInitSettings().on_finish = () => resolve(true);
                // eslint-disable-next-line no-undef
                jsPsych.run(my_timeline);
            });
        };

        start_jspsych().then(() => {
            expect(probe_sound.db[0]).toBeCloseTo(-69.5);
            expect(second_probe_sound.db[0]).toBeCloseTo(-20.5);

            // eslint-disable-next-line no-undef
            const jspsych_output_data = jsPsych.data.get().values();
            const st_data = jspsych_output_data[0];
            const st_data_2 = jspsych_output_data[2];

            expect(st_data.result.final_estimate).toBe(-70);
            expect(st_data.individual_trials).toHaveLength(12);

            expect(st_data_2.result.final_estimate).toBe(-20.5);
            expect(st_data_2.individual_trials).toHaveLength(7);

            expect(st_data.trial_arguments).not.toHaveProperty('sound');
            expect(st_data_2.trial_arguments).not.toHaveProperty('sound');
        }).then(() => {
            done();
        });
    }, 20000);
});
