# @thht_jspsych/sound_threshold

jsPsych plugin to get the perceptual threshold of a sound using maximum likelihood threshold hunting according
to [Green 1993](https://doi.org/10.1121/1.406696).

This version only works with jsPsych 7.0.0 and later. If you still need to use jsPsych 6.4, please use version 0.0.6.

# How to install

The package is on npm:

```
npm install @thht_jspsych/sound_threshold
```

# How to use

## Add necessary css

This plugin uses the [Bootstrap](https://getbootstrap.com/). So you need to include the fonts.

Include it in your entry javascript file:

 ```javascript
import 'bootstrap/dist/css/bootstrap.css';
 ```

## Use the plugin

Now you can use it as any other jsPsych plugin:

```javascript
import { Sine } from "@thht/sounds";
import SoundThresholdPlugin from '@thht_jspsych/sound_threshold';

const probe_sound = Sine(440, 1);

jsPsych.init({
    timeline: [
        {
            type: SoundThresholdPlugin,
            sound: () => probe_sound,
            db_range: [-80, -20],
        },
    ]
});
```

After the plugin has run, the sound that it used will be at the threshold volume.

The plugin offers the following parameters:

### sound
A function returning the sound to be probed. It might sound weird, that this needs to be a function, but it does
not work otherwise. Look at the example for the most simple way to do it.

### db_range
The range of volume in dB to probe. Defaults to `[-120, -20]`.

### delay
The delay from the onset of each probe trial to the onset of the sound in seconds. Defaults to `0.5`.

### question
The string to present as the question. Defaults to `'Did you hear the sound?'`.

### yes_response
The string to present as the positive response option. Defaults to `'Yes'`.

### no_response
The string to present as the negative response option. Defaults to `'No'`.
