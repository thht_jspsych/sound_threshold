// eslint-disable-next-line no-use-before-define
import React from 'react';
import {
    JsPsych, JsPsychPlugin, ParameterType,
} from 'jspsych';
import { MLThresholdHunter } from '@thht/ml_threshold';
import ReactDom from 'react-dom';
import { range } from 'mathjs';
import { Question } from './components/question.jsx';

const info = {
    name: 'sound_threshold',
    parameters: {
        sound: {
            type: ParameterType.FUNCTION,
            default: undefined,
        },
        db_range: {
            type: ParameterType.OBJECT,
            default: [-120, -20],
        },
        delay: {
            type: ParameterType.FLOAT,
            default: 0.5,
        },
        yes_response: {
            type: ParameterType.STRING,
            default: 'Yes',
        },
        no_response: {
            type: ParameterType.STRING,
            default: 'No',
        },
        question: {
            type: ParameterType.STRING,
            default: 'Did you hear the sound?',
        },
    },
};

type Info = typeof info;

class SoundThresholdPlugin implements JsPsychPlugin<Info> {
    static info = info;
    private current_trial_data: {};
    public threshold_hunter: MLThresholdHunter;
    private trialinfo: any;
    private display_element: null;
    public sound: any;
    public data: { result: {
            n_trials: number;
            converged: boolean;
            final_estimate: number;
        }; individual_trials: any[]; trial_arguments: any };
    private delay: any;

    constructor(private jsPsych: JsPsych) {}  // eslint-disable-line

    trial(display_element, trial) {
        this.data = {
            trial_arguments: null,
            individual_trials: [],
            result: {
                n_trials: undefined,
                converged: false,
                final_estimate: undefined,
            },
        };
        this.trialinfo = trial;
        this.display_element = display_element;
        this.data.trial_arguments = trial;
        this.sound = trial.sound();
        this.delay = trial.delay;
        this.threshold_hunter = new MLThresholdHunter(trial.db_range[0],
            // @ts-ignore
            range(...trial.db_range, 0.5),
            [0],
            [0.5]);

        this.play_sound();
    }

    play_sound() {
        ReactDom.render(<h1>+</h1>, this.display_element);
        this.sound.db = this.threshold_hunter.current_probe_value;

        const audio_ctx = this.sound.webaudio_context;
        const snd_obj = this.sound.start(audio_ctx.currentTime + this.delay);

        snd_obj.onended = () => this.show_question(this);
    }

    show_question(self) {
        ReactDom.render(
            <Question
                on_response={(response) => self.evaluate_response(response)}
                question={self.trialinfo.question}
                yes_response={self.trialinfo.yes_response}
                no_response={self.trialinfo.no_response}
            />,
            self.display_element,
        );
    }

    evaluate_response(response) {
        const cur_data = {
            n_trial: this.threshold_hunter.n_trials,
            cur_std: this.threshold_hunter.std_last_trials,
            answer: response === this.trialinfo.yes_response,
            current_guess: this.threshold_hunter.current_guess,
            current_probe_value: this.threshold_hunter.current_probe_value,
        };

        this.data.individual_trials.push(cur_data);

        this.threshold_hunter.process_response(response === this.trialinfo.yes_response);

        if (this.threshold_hunter.stop) {
            this.data.result.final_estimate = this.threshold_hunter.current_guess;
            this.data.result.converged = this.threshold_hunter.converged;
            this.data.result.n_trials = this.threshold_hunter.n_trials;
            this._end_trial();
        } else {
            this.play_sound();
        }
    }

    _end_trial() {
        ReactDom.unmountComponentAtNode(this.display_element);
        delete this.data.trial_arguments.sound;
        this.jsPsych.finishTrial(this.data);
    }
}

export default SoundThresholdPlugin;
