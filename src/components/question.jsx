// eslint-disable-next-line no-use-before-define
import React from 'react';
import PropTypes from 'prop-types';
import {
    Button, Col, Container, Row,
} from 'react-bootstrap';

class Question extends React.Component {
    constructor(props) {
        super(props);

        this.handle_response = this.handle_response.bind(this);
    }

    handle_response(evt, response) {
        this.props.on_response(response);
    }

    render() {
        return (
            <Container>
                <Row>
                    <h3>{this.props.question}</h3>
                </Row>
                <Row>
                    <Col>
                        {/* eslint-disable-next-line max-len */}
                        <Button onClick={(evt) => this.handle_response(evt, this.props.yes_response)}>{this.props.yes_response}</Button>
                    </Col>
                    <Col>
                        {/* eslint-disable-next-line max-len */}
                        <Button onClick={(evt) => this.handle_response(evt, this.props.no_response)}>{this.props.no_response}</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

Question.propTypes = {
    no_response: PropTypes.string.isRequired,
    on_response: PropTypes.func.isRequired,
    question: PropTypes.string,
    yes_response: PropTypes.string.isRequired,
};

export { Question };
