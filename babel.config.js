module.exports = (api) => {
    const is_test = api.env('test');

    let cfg = {};

    if (is_test) {
        cfg = {
            presets: [
                ['@parcel/babel-preset-env', { targets: { node: 'current' } }],
                '@babel/preset-typescript',
                '@babel/preset-react',
            ],
        };
    }

    return cfg;
};
